<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Pengajuan Cuti Online</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
	<link rel="stylesheet" href="{{ asset('Template/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('Template/bootstrap/css/bootstrap.css') }}">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="{{ asset('Template/dist/css/font-awesome.min.css') }}">
	<!-- Ionicons -->
	<link rel="stylesheet" href="{{ asset('Template/dist/css/ionicons.min.css') }}">
	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('Template/dist/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" href="{{ asset('Template/dist/css/AdminLTE.css') }}">
	<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="{{ asset('Template/dist/css/skins/_all-skins.min.css') }}">
	<!-- iCheck -->
	<link rel="stylesheet" href="{{ asset('Template/plugins/iCheck/flat/blue.css') }}">
	<link rel="stylesheet" href="{{ asset('Template/plugins/iCheck/square/blue.css') }}">
	<!-- Morris chart -->
	<link rel="stylesheet" href="{{ asset('Template/plugins/morris/morris.css') }}">
	<!-- jvectormap -->
	<link rel="stylesheet" href="{{ asset('Template/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
	<!-- Date Picker -->
	<link rel="stylesheet" href="{{ asset('Template/plugins/datepicker/bootstrap-datetimepicker.min.css') }}">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="{{ asset('Template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js') }}"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-red fixed sidebar-mini">
<div class="wrapper">
	<header class="main-header">
		<a href="index.php" class="logo"><span class="logo-mini">CUTI</span><span class="logo-lg"><b>Cuti</b> ONLINE</span></a>
		<nav class="navbar navbar-static-top" role="navigation">
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><span class="sr-only">Toggle navigation</span></a>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="{{ asset('Template/dist/img/profile/no-image.jpg') }}" class='user-image' alt='User Image'>
							<span class="hidden-xs">Aplikasi Pengajuan Cuti Online</span>
						</a>
						<ul class="dropdown-menu">
							<li class="user-header">
								<img src="{{ asset('Template/dist/img/profile/no-image.jpg') }}" class='img-circle' alt='User Image'>
								<p>Aplikasi Pengajuan Cuti Online</p>
							</li>
							<li class="user-body">
								<div class="row">
									<div class="col-xs-12 text-center">
									</div>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="content-wrapper">
        <div class="login-box">
            <div class="login-logo">
                <a href="index.php"><b>cuti</b> ONLINE</a>
            </div>
            <div class="box box-primary">
                <div class="login-box-body">
                    <p class="login-box-msg">Login To Access Your Autorization</p>
                    <form action="index.php?page=act-login&op=in" method="POST">
                        <div class="form-group has-feedback">
                            <input type="text" name="id_user" class="form-control" placeholder="User Name"><span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="password" class="form-control" placeholder="Password"><span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-8"></div>
                            <div class="col-xs-4">
                            <button type="submit" class="btn btn-danger btn-block btn-flat">Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
	</div>
	<footer class="main-footer">
		<div class="pull-right hidden-xs"><b>Version</b> 1.0</div>
		Copyright &copy; 2016 <a href="#" target="_blank">cuti ONLINE</a>. All rights reserved
	</footer>
</div>
	<!-- ./wrapper -->
	<!-- jQuery 2.1.4 -->
	<script src="{{ asset('Template/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') }}"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="{{ asset('Template/bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- Morris.js charts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') }}"></script>
	<script src="{{ asset('Template/plugins/morris/morris.min.js') }}"></script>
	<!-- Sparkline -->
	<script src="{{ asset('Template/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
	<!-- jvectormap -->
	<script src="{{ asset('Template/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
	<script src="{{ asset('Template/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
	<!-- jQuery Knob Chart -->
	<script src="{{ asset('Template/plugins/knob/jquery.knob.js') }}"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="{{ asset('Template/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
	<!-- Slimscroll -->
	<script src="{{ asset('Template/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
	<!-- FastClick -->
	<script src="{{ asset('Template/plugins/fastclick/fastclick.js') }}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('Template/dist/js/app.min.js') }}"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="{{ asset('Template/dist/js/pages/dashboard.js') }}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('Template/dist/js/demo.js') }}"></script>
</body>
</html>