<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;
    protected $table = 'tb_pegawai';
    protected $fillable = [
        'nip','nama','jk','jab','tmp_lhr','tgl_lhr','gol_darah','agama','status','telp','alamat','hak_cuti_tahunan'
    ];
}
