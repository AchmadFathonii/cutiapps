<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\homeAdminController;
use App\Http\Controllers\homeHRDController;
use App\Http\Controllers\homePegawaiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Login', [LoginController::class, 'index']);
Route::get('/DashboardAdmin', [homeAdminController::class, 'index']);
Route::get('/DashboardHRD', [homeHRDController::class, 'index']);
Route::get('/DashboardPegawai', [homePegawaiController::class, 'index']);